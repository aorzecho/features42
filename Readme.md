# Features 42

Serving features metadata and previews.

The project is api-first. Api specification is defined using [OpenAPI](https://swagger.io/specification/).

## code structure
- `spec` module is responsible for the api specification and code generation (skeleton app)
- `app` module in the service implementation

## build & run
Using [maven wrapper](https://github.com/takari/maven-wrapper)

```
  $ ./mvnw clean package 
  $ java -jar app/target/app-*.jar
```
go to [localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html) to get the api documentation

## notes
The current version serves data from included geojson file.
In this version:
- loading on startup and serving all data from memory (the dataset is small))
- parsing only minimal subset of metadata