package org.aorzecho.features42;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class Features42Test {

    private static final String FEATURES_PATH = "/features/";
    private static final UUID featureId = UUID.fromString("39c2f29e-c0f8-4a39-a98b-deed547d6aea");

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void listFeatures_isSuccess() throws Exception {
        this.mockMvc.perform(get(FEATURES_PATH))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    public void getFeature_isSuccess() throws Exception {
        this.mockMvc.perform(get(FEATURES_PATH + featureId))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    public void getFeature_invalidId_is400() throws Exception {
        this.mockMvc.perform(get(FEATURES_PATH + "notReallyUUID"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void getPreview_isSuccess() throws Exception {
        this.mockMvc.perform(get(FEATURES_PATH + featureId + "/quicklook"))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    public void getPreview_invalidId_is400() throws Exception {
        this.mockMvc.perform(get(FEATURES_PATH + "notReallyUUID/quicklook"))
                .andExpect(status().is4xxClientError());
    }
}
