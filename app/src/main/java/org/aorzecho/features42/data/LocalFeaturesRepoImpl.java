package org.aorzecho.features42.data;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import org.aorzecho.features42.api.Feature;
import org.checkerframework.checker.nullness.Opt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentSkipListMap;

@Service
public class LocalFeaturesRepoImpl implements FeaturesRepo {

    private static Logger logger = LoggerFactory.getLogger(LocalFeaturesRepoImpl.class);

    private Gson gson = new Gson();

    private final ConcurrentMap<UUID, Feature> features = new ConcurrentSkipListMap<>();
    private final ConcurrentMap<UUID, byte[]> previews = new ConcurrentSkipListMap<>();


    @Override
    public Feature findById(UUID id) {
        return features.get(id);
    }

    @Override
    public List<Feature> findAll() {
        return new ArrayList<>(features.values());
    }

    @Override
    public byte[] getPreview(UUID id) {
        return previews.get(id);
    }


    @PostConstruct
    private void loadData() throws IOException {

        JsonReader reader = new JsonReader(
                new InputStreamReader(new ClassPathResource("source-data.json").getInputStream(), StandardCharsets.UTF_8)
        );

        reader.beginArray();
        while (reader.hasNext()) {
            GeoFeatureCollection fc = gson.fromJson(reader, GeoFeatureCollection.class);
            Optional.ofNullable(fc)
                    .filter(f -> "FeatureCollection".equals(f.type))
                    .map(f -> f.features)
                    .ifPresent(this::indexFeatures);
        }

        logger.info("Loaded features from file, {}  found", features.size());
        logger.info("Loaded previews from file, {}  found", previews.size());
    }

    private void indexFeatures(Collection<GeoFeature> geoFeatures) {
        geoFeatures.stream()
                .filter(gf -> gf.properties != null && gf.properties.id != null)
                .forEach(gf -> {
                            addFeature(gf);
                            addPreview(gf.properties);
                        }
                );
    }

    private void addFeature(GeoFeature gf) {
        Feature feature = new Feature()
                .id(gf.properties.id)
                .timestamp(gf.properties.timestamp);
        Optional.ofNullable(gf.properties.acquisition).ifPresent(
                acquisition -> feature
                        .beginViewingDate(acquisition.beginViewingDate)
                        .endViewingDate(acquisition.endViewingDate)
                        .missionName(acquisition.missionName)
        );
        features.put(feature.getId(), feature);
    }

    private void addPreview(GeoFeatureProps props) {
        if (props.quicklook != null) {
            previews.put(props.id, Base64.getDecoder().decode(props.quicklook));
        } else {
            logger.warn("No preview for feature {}", props.quicklook);
        }
    }

    //==== classes used for parsing
    private static class GeoFeatureAcquisition {
        String missionName;
        long beginViewingDate, endViewingDate;
    }

    private static class GeoFeatureProps {
        UUID id;
        long timestamp;
        String quicklook;
        GeoFeatureAcquisition acquisition;

        @Override
        public String toString() {
            return "GeoFeatureProps{" +
                    "id=" + id +
                    ", timestamp=" + timestamp +
                    '}';
        }
    }

    private static class GeoFeature {
        String type;
        GeoFeatureProps properties;

        @Override
        public String toString() {
            return "GeoFeature{" +
                    "type='" + type + '\'' +
                    ", properties=" + properties +
                    '}';
        }
    }

    private static class GeoFeatureCollection {
        String type;
        Collection<GeoFeature> features;

        @Override
        public String toString() {
            return "GeoFeatureCollection{" +
                    "type='" + type + '\'' +
                    ", features=" + features +
                    '}';
        }
    }

}
