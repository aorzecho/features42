package org.aorzecho.features42.data;

import org.aorzecho.features42.api.Feature;

import java.util.List;
import java.util.UUID;

public interface FeaturesRepo {
    Feature findById(UUID id);

    List<Feature> findAll();

    byte[] getPreview(UUID id);
}
