package org.aorzecho.features42.service;

import org.aorzecho.features42.api.Feature;
import org.aorzecho.features42.api.FeaturesApiDelegate;
import org.aorzecho.features42.data.LocalFeaturesRepoImpl;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class FeatureService implements FeaturesApiDelegate {

    private final LocalFeaturesRepoImpl featuresRepo;

    public FeatureService(LocalFeaturesRepoImpl featuresRepo) {
        this.featuresRepo = featuresRepo;
    }

    @Override
    public ResponseEntity<Feature> getFeatureById(UUID id) {
        return
            Optional.ofNullable(featuresRepo.findById(id))
                .map(ResponseEntity::ok)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @Override
    public ResponseEntity<Resource> getFeaturePreview(UUID id) {
        return
                Optional.ofNullable(featuresRepo.getPreview(id))
                        .map(bytes -> (Resource) new ByteArrayResource(bytes))
                        .map(ResponseEntity::ok)
                        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
//        return ResponseEntity.ok(new ClassPathResource("Answer_to_Life.png"));
    }

    @Override
    public ResponseEntity<List<Feature>> listFeatures() {
        return ResponseEntity.ok(featuresRepo.findAll());
    }
}
